package com.csi436.movieshelf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.csi436.movieshelf.data.MovieItem;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

public class Activity_Add_Movie extends ListActivity
{

	private List<MovieItem> allMovies = new ArrayList<MovieItem>();
	private SearchAdapter m_Adapter;
	Context onCreateContext = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_movie);
        getActionBar().setDisplayHomeAsUpEnabled(true);

		this.m_Adapter = new SearchAdapter(this, R.layout.listitem_movie_search, allMovies);
		setListAdapter(this.m_Adapter);
		
		onCreateContext = this;
		
		
		SearchView searchView = (SearchView) findViewById(R.id.addMovieSearchView);
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener( ) 
		{
			final Context outsideContext = onCreateContext;
		    private Timer timer = new Timer();
		    
		    @Override
		    public boolean onQueryTextChange(String newText) 
		    {
                timer.cancel();
                timer = new Timer();
                

		    	while(newText.startsWith(" ") || newText.endsWith(" "))
		    	{
		    		newText = newText.trim();
		    	}
		    	
		    	if(newText.length() != 0 || !newText.equals(""))
		    	{
			    	final String passedText = newText.replaceAll("[^a-zA-Z0-9\\s]", "");

	                timer.schedule(new TimerTask() {
	                    @Override
	                    public void run() 
	                    {
	    			    	new searchMovies(outsideContext, passedText).execute();
	                    }

	                }, 500);
		    	}
		        return true;
		    }

		    @Override
		    public boolean onQueryTextSubmit(String newText) {
		    	timer.cancel();
                timer = new Timer();
                

		    	while(newText.startsWith(" ") || newText.endsWith(" "))
		    	{
		    		newText = newText.trim();
		    	}
		    	
		    	if(newText.length() != 0 || !newText.equals(""))
		    	{
			    	final String passedText = newText.replaceAll("[^a-zA-Z0-9\\s]", "");

	                timer.schedule(new TimerTask() {
	                    @Override
	                    public void run() 
	                    {
	    			    	new searchMovies(outsideContext, passedText).execute();
	                    }

	                }, 500);
		    	}
		        return true;
		    }
		});
		
		
	}
	
	@Override
    protected void onListItemClick(ListView l, View v, int position, long id)
    {
    	super.onListItemClick(l, v, position, id);
    	
    	MovieItem movie = (MovieItem) getListAdapter().getItem(position);
    	
    	Intent intent = new Intent(this, Activity_Search_Details.class);
    	intent.putExtra("Movie", movie);
    	startActivity(intent);
    	
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
			case android.R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_add_movie, menu);
		return true;
	}
	
	
	private class searchMovies extends AsyncTask<Void, Void, Boolean>
	{
		Context context;
		String movieTitle = "";
		
		public searchMovies(Context newContext, String titleName)
		{
			context = newContext;
			movieTitle = titleName;
		}
		
		@Override
		protected Boolean doInBackground(Void... params)
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			
			try
			{
				SharedPreferences mPreferences = getSharedPreferences("com.csi436.movieshelf", Context.MODE_PRIVATE);
		        int userID = mPreferences.getInt("userID", -1);
		        String ipAddress = mPreferences.getString("server_ip", "192.168.1.100");
		        
		        if(userID == -1)
		        	return false;
		        
		        movieTitle = movieTitle.replaceAll(" ", "%20");
				String url = "http://" + ipAddress + ":58500/NewMovie/" + movieTitle;
				
				Log.i("Url", url);
				HttpGet getthatSite = new HttpGet(url);
				response = httpclient.execute(getthatSite);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK)
				{
					BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
					String json = reader.readLine();
					
					if(json.isEmpty())
						return false;
					//Log.i("Response", json);
					
					try
					{
						JSONObject rottenReturn = new JSONObject(json);
						JSONArray allMovieInfo = rottenReturn.getJSONArray("MovieInfo");
						allMovies.clear();
						for(int i = 0; i < allMovieInfo.length(); i++)
						{	
							MovieItem newMovie = new MovieItem();
							
							JSONObject movie = allMovieInfo.getJSONObject(i);
							newMovie.setTitle(movie.getJSONObject("Movie").get("Title").toString());
							newMovie.setDescription(movie.getJSONObject("Movie").get("Description").toString());
							newMovie.setRTID(movie.getJSONObject("Movie").getInt("RTID"));
							if(movie.getJSONObject("Movie").isNull("Url"))
							{
								newMovie.setPhotoURL("");
							}
							else
								newMovie.setPhotoURL(movie.getJSONObject("Movie").get("Url").toString());
							
							if(!movie.getJSONObject("Movie").isNull("Year"))
								newMovie.setYear(movie.getJSONObject("Movie").getInt("Year"));
							
							JSONArray actors = movie.getJSONArray("Actors");
							
							if(actors.length() >= 1)
								newMovie.setActorOne(actors.getJSONObject(0).getString("Name").toString());
							else
								newMovie.setActorOne("");
							
							if(actors.length() >= 2)
								newMovie.setActorTwo(actors.getJSONObject(1).getString("Name").toString());
							else
								newMovie.setActorOne("");
							
							allMovies.add(newMovie);
						}
						
						
						return true;
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					
				}
				else
				{
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
			} 
			catch (ClientProtocolException e)
			{
				return false;
			}
			catch (IOException e)
			{
				return false;
			}
			
			return false;
		}
		
		
		@Override
		protected void onPostExecute(final Boolean success)
		{
			if(success)
			{
				//MovieAdapter adapter = new MovieAdapter(a, R.layout.listitem_movie_home, allMovies);
				m_Adapter.notifyDataSetChanged();
			}
			else
			{
				Toast.makeText(context, "Something went wrong querying movies", Toast.LENGTH_SHORT).show();
			}
		}
		
	}

}
