package com.csi436.movieshelf;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class Activity_Login extends Activity
{
	/**
	 * A dummy authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.
	 */
	private static final String[] DUMMY_CREDENTIALS = new String[] {
			"foo@example.com:hello", "bar@example.com:world" };

	/**
	 * The default email to populate the email field with.
	 */
	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	//private UserLoginTask mAuthTask = null;

	// Values for email and password at the time of the login attempt.
	private String mEmail;
	private String mPassword;

	// UI references.
	private EditText mEmailView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);

		// Set up the login form.
		mEmail = getIntent().getStringExtra(EXTRA_EMAIL);
		mEmailView = (EditText) findViewById(R.id.activity_login_email);
		mEmailView.setText(mEmail);

		mPasswordView = (EditText) findViewById(R.id.activity_login_password);
		mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener()
				{
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent)
					{
						if (id == R.id.login || id == EditorInfo.IME_NULL)
						{
							//attemptLogin();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		/*findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener()
				{
					@Override
					public void onClick(View view)
					{
						attemptLogin();
					}
				});*/
		
		findViewById(R.id.activity_login_button_sign_up).setOnClickListener(new View.OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				attemptSignUp();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.activity_login, menu);
		return true;
	}
	/*
	public void attemptLogin()
	{
		if (mAuthTask != null)
		{
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword))
		{
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail))
		{
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel)
		{
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else
		{
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}*/

	
	public void attemptSignUp()
	{
		EditText usernameEditText = (EditText) findViewById(R.id.activity_login_userName);
		EditText passwordEditText = (EditText) findViewById(R.id.activity_login_password);
		EditText emailEditText = (EditText) findViewById(R.id.activity_login_email);
		EditText ipEditText = (EditText) findViewById(R.id.activity_login_ipAddress);
		
		
		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword))
		{
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail))
		{
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel)
		{
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else
		{
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			String username, password, email, ipaddress;
			username = usernameEditText.getText().toString();
			password = passwordEditText.getText().toString();
			email = emailEditText.getText().toString();
			ipaddress = ipEditText.getText().toString();
			
			
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			new UserSignUpTask(username, password, email, ipaddress).execute();
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show)
	{
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
		{
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter()
					{
						@Override
						public void onAnimationEnd(Animator animation)
						{
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter()
					{
						@Override
						public void onAnimationEnd(Animator animation)
						{
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else
		{
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	/*
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean>
	{
		String responseString = null;
		
		@Override
		protected Boolean doInBackground(Void... params)
		{
			// TODO: attempt authentication against a network service.
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			
			try
			{
				HttpGet getthatSite = new HttpGet("http://192.168.1.134:58500/Users");
				getthatSite.addHeader("UserName", "Ryan4");
				getthatSite.addHeader("Password", "123456");
				getthatSite.addHeader("Email", "blank@gmail.com");
				getthatSite.addHeader("New", "1");
				response = httpclient.execute(getthatSite);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK)
				{
					BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
					responseString = reader.readLine();
					responseString = responseString.replaceAll("\"", "");
					Log.i("Response", responseString);
					//If returns -1, there's a problem
					//If returns something else is the new ID
				}
				else
				{
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
				// Simulate network access.
				//Thread.sleep(2000);
			} 
			catch (ClientProtocolException e)
			{
				return false;
			}
			catch (IOException e)
			{
				return false;
			}
			
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success)
		{
			mAuthTask = null;
			showProgress(false);

			if (success)
			{
				//If successful login launch home activity as well as save the preference
				
				//Verify that the user logged in and disable the login screen from launching
		    	SharedPreferences mPreferences = getSharedPreferences("com.csi436.movieshelf", Context.MODE_PRIVATE);
		    	SharedPreferences.Editor editor = mPreferences.edit();
	        	editor.putBoolean("firstTime", false);
	        	editor.putInt("userID", Integer.parseInt(responseString));
	        	editor.commit();
				
				
				Intent intentHome = new Intent(Activity_Login.this, Activity_Home.class);
				startActivity(intentHome);
				finish();
			} 
			else
			{
				mPasswordView.setError(getString(R.string.error_incorrect_password));
				mPasswordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled()
		{
			mAuthTask = null;
			showProgress(false);
		}
	}
	*/
	
	public class UserSignUpTask extends AsyncTask<Void, Void, Boolean>
	{
		String responseString = null;
		
		String User_username;
		String User_password;
		String User_email;
		String User_ip;
		
		public UserSignUpTask(String userName, String password, String email, String ipAddress)
		{
			userName = userName.trim();
			userName = userName.replaceAll(" ", "");
			email = email.trim();
			ipAddress = ipAddress.trim();
			User_username = userName;
			User_password = password;
			User_email = email;
			User_ip = ipAddress;
		}
		
		@Override
		protected Boolean doInBackground(Void... params)
		{
			// TODO: attempt authentication against a network service.
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			
			try
			{
				HttpGet getthatSite = new HttpGet("http://" + User_ip + ":58500/Users");
				getthatSite.addHeader("UserName", User_username);
				getthatSite.addHeader("Password", User_password);
				getthatSite.addHeader("Email", User_email);
				getthatSite.addHeader("New", "1");
				response = httpclient.execute(getthatSite);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK)
				{
					BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
					responseString = reader.readLine();
					responseString = responseString.replaceAll("\"", "");
					Log.i("Response", responseString);
					//If returns -1, there's a problem
					//If returns something else is the new ID
				}
				else
				{
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
				// Simulate network access.
				//Thread.sleep(2000);
			} 
			catch (ClientProtocolException e)
			{
				return false;
			}
			catch (IOException e)
			{
				return false;
			}
			
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success)
		{
			//mAuthTask = null;
			showProgress(false);

			if (success)
			{
				//If successful login launch home activity as well as save the preference
				
				//Verify that the user logged in and disable the login screen from launching
		    	SharedPreferences mPreferences = getSharedPreferences("com.csi436.movieshelf", Context.MODE_PRIVATE);
		    	SharedPreferences.Editor editor = mPreferences.edit();
	        	editor.putBoolean("firstTime", false);
	        	editor.putInt("userID", Integer.parseInt(responseString));
	        	editor.putString("server_ip", User_ip);
	        	editor.commit();
				
				
				Intent intentHome = new Intent(Activity_Login.this, Activity_Home.class);
				startActivity(intentHome);
				finish();
			} 
			else
			{
				mPasswordView.setError(getString(R.string.error_incorrect_password));
				mPasswordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled()
		{
			//mAuthTask = null;
			showProgress(false);
		}
	}
	
	
	public void launchOffline(View v)
	{
		Intent intentHome = new Intent(Activity_Login.this, Activity_Home.class);
		intentHome.putExtra("Offline", true); //Send flag to activity that we are launching in offline mode
		startActivity(intentHome);
		finish();
	}
	/**End of Activity**/
}
