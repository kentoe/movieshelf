package com.csi436.movieshelf;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.csi436.movieshelf.data.MovieItem;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NavUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Activity_Search_Details extends Activity
{
	MovieItem passedInMovie;
	ArrayList<MovieItem> listOfUserMovies = new ArrayList<MovieItem>();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        
        passedInMovie = (MovieItem) getIntent().getSerializableExtra("Movie");
        
        /***Details items***/
        TextView details_Title = (TextView) findViewById(R.id.detailsTextViewTitle);
        TextView details_Year = (TextView) findViewById(R.id.detailsTextViewYear);
        TextView details_Director = (TextView) findViewById(R.id.detailsTextViewDirector);
        TextView details_Genre = (TextView) findViewById(R.id.detailsTextViewGenre);
        TextView details_Description = (TextView) findViewById(R.id.detailsTextViewDescription);
        details_Description.setMovementMethod(new ScrollingMovementMethod());
        
        details_Title.setText(passedInMovie.getTitle());
        details_Year.setText(Integer.toString(passedInMovie.getYear()));
        details_Director.setText(passedInMovie.getDirector());
        details_Genre.setText(passedInMovie.getGenre());
        details_Description.setText(passedInMovie.getDescription());
        
        //***Check whether this movie is in the user's library****/
        //Get RTID of users movies, then check against this movie what button should be shown
        new getMovies(passedInMovie.getTitle()).execute();
        
        
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_search_details, menu);
		return true;
	}
	

    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    
    public void addMovie(View v)
    {
    	new addMovie(this).execute();
    }
    
    public void removeMovie(View v)
    {
    	new deleteMovie(this).execute();
    }
    
    private class getMovies extends AsyncTask<Void, Void, Boolean>
	{
    	String titleOfMovie;

    	ArrayList<String> listOfTitles = new ArrayList<String>();
    	
    	public getMovies(String movieTitle)
    	{
    		titleOfMovie = movieTitle;
    	}
		
		@Override
		protected Boolean doInBackground(Void... params)
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			
			try
			{
				SharedPreferences mPreferences = getSharedPreferences("com.csi436.movieshelf", Context.MODE_PRIVATE);
		        int userID = mPreferences.getInt("userID", -1);
		        String ipAddress = mPreferences.getString("server_ip", "192.168.1.100");
		        
		        if(userID == -1)
		        	return false;
		        
		        //Just getting the ID's
				
				HttpGet getthatSite = new HttpGet("http://" + ipAddress + ":58500/Users/"+ Integer.toString(userID) + "/Movies");
				response = httpclient.execute(getthatSite);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK)
				{
					BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
					String json = reader.readLine();
					JSONTokener tokener = new JSONTokener(json);
					try
					{
						listOfTitles.clear();
						
						JSONArray finalResult = new JSONArray(tokener);
						for(int i = 0; i < finalResult.length(); i++)
						{
							listOfTitles.add(finalResult.getJSONObject(i).get("Title").toString());
							
							//Get rtid and user ID of movie
							MovieItem newMovie = new MovieItem();
							newMovie.setTitle(finalResult.getJSONObject(i).get("Title").toString());
							newMovie.setLibraryId(finalResult.getJSONObject(i).getInt("Id"));
							newMovie.setRTID(finalResult.getJSONObject(i).getInt("RTID"));
							listOfUserMovies.add(newMovie);
						}
						
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					
					
				}
				else
				{
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
				
				for(int i = 0; i < listOfTitles.size(); i++)
				{
					String indvidiualMovie = listOfTitles.get(i).toString();
					
					//If the movie passed equals a movie in our list, return true
					if(titleOfMovie.equals(indvidiualMovie))
					{
						return true;
					}
				}
				
				
			}
			catch (ClientProtocolException e)
			{
				return false;
			}
			catch (IOException e)
			{
				return false;
			}
			
			return false;
		}
		
		@Override
		protected void onPostExecute(final Boolean success)
		{
			//If passed in true -> Movie exists thus remove it
			//If passed in false -> Movie does not exist thus add it
			if(success)
			{
				Button removeMovie = (Button) findViewById(R.id.detailsButtonRemoveMovie);
				removeMovie.setVisibility(View.VISIBLE);
			}
			else
			{
				Button addMovie = (Button) findViewById(R.id.detailsButtonAddMovie);
				addMovie.setVisibility(View.VISIBLE);
			}
		}
		
	}

    private class addMovie extends AsyncTask<Void, Void, Boolean>
    {
    	Context context;
    	
		public addMovie(Context c)
		{
			context = c;
		}
		@Override
		protected Boolean doInBackground(Void... params)
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			
			try
			{
				SharedPreferences mPreferences = getSharedPreferences("com.csi436.movieshelf", Context.MODE_PRIVATE);
		        int userID = mPreferences.getInt("userID", -1);
		        String ipAddress = mPreferences.getString("server_ip", "192.168.1.100");
		        
		        if(userID == -1)
		        	return false;
		        
		        //Just getting the ID's
				
				HttpGet getthatSite = new HttpGet("http://" + ipAddress + ":58500/Users/"+ Integer.toString(userID) + "/Movies/New/" + passedInMovie.getRTID());
				response = httpclient.execute(getthatSite);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK)
				{
					BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
					String json = reader.readLine();
					Log.i("Response", json);
					
					return true;
					
				}
				else
				{
					return false;
				}
				
				
			}
			catch (ClientProtocolException e)
			{
				return false;
			}
			catch (IOException e)
			{
				return false;
			}
		}
		
		@Override
		protected void onPostExecute(final Boolean success)
		{
			if(success)
			{
				Toast.makeText(context, "Successfully added movie", Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(context, Activity_Home.class);
				finish();
				startActivity(intent);
			}
			else
			{
				Toast.makeText(context, "Oops, could not add movie", Toast.LENGTH_SHORT).show();
			}
				
			
		}
    }

    private class deleteMovie extends AsyncTask<Void, Void, Boolean>
    {
    	Context context;
    	
		public deleteMovie(Context c)
		{
			context = c;
		}
		@Override
		protected Boolean doInBackground(Void... params)
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			
			try
			{
				SharedPreferences mPreferences = getSharedPreferences("com.csi436.movieshelf", Context.MODE_PRIVATE);
		        int userID = mPreferences.getInt("userID", -1);
		        String ipAddress = mPreferences.getString("server_ip", "192.168.1.100");
		        
		        if(userID == -1)
		        	return false;
		        
		        int libraryID = 0;
		        
		        //Get the libraryID
		        for(int i = 0; i < listOfUserMovies.size(); i++)
		        {
		        	int RTID = listOfUserMovies.get(i).getRTID();
		        	if(RTID == passedInMovie.getRTID())
		        	{
		        		libraryID = listOfUserMovies.get(i).getLibraryId();
		        	}
		        }
		        
				String url = "http://" + ipAddress + ":58500/Users/"+ Integer.toString(userID) + "/Movies/" + libraryID + "/DeleteMovie/";
				Log.i("Url", url);
				HttpGet getthatSite = new HttpGet(url);
				response = httpclient.execute(getthatSite);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK)
				{
					BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
					String json = reader.readLine();
					Log.i("Response", json);
					
					if(json.equals("Movie does not exist."))
						return false;
					
					return true;
					
				}
				else
				{
					return false;
				}
				
				
			}
			catch (ClientProtocolException e)
			{
				return false;
			}
			catch (IOException e)
			{
				return false;
			}
		}
		
		@Override
		protected void onPostExecute(final Boolean success)
		{
			if(success)
			{
				Toast.makeText(context, "Successfully removed movie", Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(context, Activity_Home.class);
				finish();
				startActivity(intent);
			}
			else
			{
				Toast.makeText(context, "Oops, could not remove movie", Toast.LENGTH_SHORT).show();
			}
				
			
		}
    }


}
