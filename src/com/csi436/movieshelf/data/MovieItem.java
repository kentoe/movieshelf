package com.csi436.movieshelf.data;

import java.io.Serializable;

public class MovieItem implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String Title, Description, ReleaseDate, Director, Genre, PhotoURL, ActorOne, ActorTwo, Comment;
	private int Rating, RTID, Year, libraryID;
	private boolean Lent = false, Viewed = false;
	
	/***** Title ****/
	public String getTitle() { return this.Title; }
	
	public void setTitle(String newTitle) { this.Title = newTitle; }
	
	
	/****** Description ****/
	public String getDescription() { return this.Description; }

	public void setDescription(String newDescription) { this.Description = newDescription; }
	
	/****** ReleaseDate ******/
	public String getReleaseDate() { return this.ReleaseDate; }
	
	public void setReleaseDate(String newDate) { this.ReleaseDate = newDate; }
	
	
	/******* Director ******/
	public String getDirector()
	{
		return this.Director;
	}
	
	public void setDirector(String newDirector)
	{
		this.Director = newDirector;
	}
	
	/***** Genre *****/
	public String getGenre()
	{
		return this.Genre;
	}
	
	public void setGenre(String newGenre)
	{
		this.Genre = newGenre;
	}
	
	/***** Photo URL ****/
	public String getPhotoURL()
	{
		return this.PhotoURL;
	}
	
	public void setPhotoURL(String newPhotoURL)
	{
		this.PhotoURL = newPhotoURL;
	}
	
	/***** Actors ******/
	public String getActorOne() { return this.ActorOne; }
	public String getActorTwo() { return this.ActorTwo; }
	
	public void setActorOne(String newActor) { this.ActorOne = newActor; }
	public void setActorTwo(String newActor) { this.ActorTwo = newActor; }
	
	/****** Rating *****/
	public int getRating()
	{
		return this.Rating;
	}
	
	public void setRating(int newRating)
	{
		this.Rating = newRating;
	}
	
	/****** RTID ******/
	public int getRTID() { return this.RTID; }
	
	public void setRTID(int newRTID) { this.RTID = newRTID; }
	
	/****** Year *****/
	public int getYear() { return this.Year; }
	
	public void setYear(int newYear) { this.Year = newYear; }
	
	/***** Library ID ****/
	public int getLibraryId() { return this.libraryID; }
	
	public void setLibraryId(int newId) { this.libraryID = newId; }
	
	
	/***** Lent *****/
	public boolean getIsLent() { return this.Lent; }
	
	public void setIsLent(boolean newLent) { this.Lent = newLent; }
	
	/**** Viewed ****/
	public boolean getIsViewed() { return this.Viewed; }
	
	public void setIsViewed(boolean newViewed) { this.Viewed = newViewed; }
	
	
	/****** Comment ****/
	public String getComment() { return this.Comment; }
	public void setComment(String newComment) { this.Comment = newComment; }
	
}
