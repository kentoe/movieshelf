package com.csi436.movieshelf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.csi436.movieshelf.data.MovieItem;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class Activity_Home extends ListActivity
{
	
	/*TODO:
	 * 
	 * Logic:
	 * 
	 * 		Activity_Add_Movie
	 *  		1. Integrate an activity that adds a movie to the database
	 * 
	 * 		Activity_Details
	 * 			1. Integrate an activity that takes in information
	 * 			2. Verify button is hidden whether or not inside collection
	 * 
	 * 		Activity_Home
	 *  		1. Integrate List View of local database (List View implemented)
	 *  		2. Implement searching functionality of list view (design done)
	 * 			3. Long press dialogs for each in particular movie listed (simple dialog)
	 *  			- Edit, Delete, Lend
	 *  		4. Filters?
	 *  		5. Sync with database
	 * 			6. Off-line mode checks (I.e. mobile data off)
	 *  
	 * 		Activity_Login
	 *  		1. Authenticate with database/REST Api
	 *  
	 */
	
	Boolean offlineMode = null;
	Context mainContext;
	
	private List<MovieItem> allMovies = new ArrayList<MovieItem>();
	private MovieAdapter m_Adapter;
	
	private static final int LEND_ID = Menu.FIRST + 1;
	private static final int VIEWED_ID = LEND_ID + 1;
	

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		Bundle intentExtras = getIntent().getExtras();
		
		this.m_Adapter = new MovieAdapter(this, R.layout.listitem_movie_home, allMovies);
		setListAdapter(this.m_Adapter);
		
		/****Checks for Offline mode****/
		//If coming from the sign up page, get the flag
		if(intentExtras != null)
			offlineMode = intentExtras.getBoolean("Offline", false);
		
		//Check if this is the first time ran
		//If offlineMode is disabled
		if(offlineMode == null || offlineMode == false)
		{
			checkFirstRun();	//Check if ran for the first time (i.e. not logged in)
			new getMovies(this).execute();
		}
		/*******************************/
		
        mainContext = this;
        registerForContextMenu(getListView());
		
		
        
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_home, menu);
		/*SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.menu_search_movies).getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setIconified(false); //Do not iconify the widget; expand it by default
		//Configure the search info and add any event listeners
		//....*/
		
		return true;
	}

	@Override
    public void onCreateContextMenu(ContextMenu menu, View v,  ContextMenuInfo menuInfo) 
    {
		super.onCreateContextMenu(menu, v, menuInfo);
		

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
        MovieItem movie = (MovieItem) getListAdapter().getItem(info.position);
        
        //Change appropriate menu's for values set
        if(movie.getIsLent() == true)
          	menu.add(0, Menu.FIRST + 1, 0, "Remove Lent");
        else if(movie.getIsLent() == false)
        	menu.add(0, Menu.FIRST + 1, 0, "Check Lent");
        
        if(movie.getIsViewed() == true)
        	menu.add(0, Menu.FIRST + 2, 0, "Remove Viewed");
        else if(movie.getIsViewed() == false)
        	menu.add(0, Menu.FIRST + 2, 0, "Check Viewed");
        
        menu.add(0, Menu.FIRST + 3, 0, "Set Rating");
		
    }
	
	@Override
    public boolean onContextItemSelected(MenuItem item) 
    {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		final MovieItem movie = (MovieItem) getListAdapter().getItem(info.position);
		
    	if(item.getTitle().equals("Remove Lent"))
    	{
    		//Remove Lending flag
    		new lendMovie(this, false, movie.getLibraryId()).execute();
    		
    		
    	}
    	else if(item.getTitle().equals("Check Lent"))
    	{
    		//Add Lending Flag
    		new lendMovie(this, true, movie.getLibraryId()).execute();
    		
    	}
    	else if(item.getTitle().equals("Remove Viewed"))
    	{
    		//Remove viewed flag
    		new viewMovie(this, false, movie.getLibraryId()).execute();
    		
    	}
    	else if(item.getTitle().equals("Check Viewed"))
    	{
    		//Add viewed Flag
    		new viewMovie(this, true, movie.getLibraryId()).execute();
    		
    	}
    	else if(item.getTitle().equals("Set Rating"))
    	{
    		// custom dialog
    		final Dialog dialog = new Dialog(mainContext);
			dialog.setContentView(R.layout.dialog_ratingbar);
			dialog.setTitle("Set Rating");
 
			// set the custom dialog components - text, image and button
			final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.dialog_rating_ratingBar);
			Button dialogButton = (Button) dialog.findViewById(R.id.dialog_rating_submitButton);
			// if button is clicked, close the custom dialog
			
			dialogButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) 
				{
					//Set Rating here
					int rated = (int) ratingBar.getRating();
					
					new rateMovie(mainContext, rated, movie.getLibraryId()).execute();
					
					dialog.dismiss();
				}
			});
 
			dialog.show();			
    	}
    	
	    return super.onContextItemSelected(item);
    }
	
	@Override
    protected void onListItemClick(ListView l, View v, int position, long id)
    {
    	super.onListItemClick(l, v, position, id);
    	
    	MovieItem movie = (MovieItem) getListAdapter().getItem(position);
    	
    	Intent intent = new Intent(this, Activity_Movie_Details.class);
    	intent.putExtra("Movie", movie);
    	startActivity(intent);
    	
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
			case R.id.menu_add_movie:
				Intent intent = new Intent(this, Activity_Add_Movie.class);
				startActivity(intent);
				return true;
			case R.id.menu_sync_movies:
				new getMovies(this).execute();
				return true;
			case R.id.menu_change_ip:
				// custom dialog
	    		final Dialog dialog = new Dialog(mainContext);
				dialog.setContentView(R.layout.dialog_changeip);
				dialog.setTitle("Change Server IP");
	 
				Button dialogButton = (Button) dialog.findViewById(R.id.dialog_changeip_submitButton);
				// if button is clicked, close the custom dialog
				
				dialogButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) 
					{
						//Set Rating here
						EditText oneEdit = (EditText) dialog.findViewById(R.id.dialog_changeip_oneEditText);
						EditText twoEdit = (EditText) dialog.findViewById(R.id.dialog_changeip_twoEditText);
						EditText threeEdit = (EditText) dialog.findViewById(R.id.dialog_changeip_threeEditText);
						EditText fourEdit = (EditText) dialog.findViewById(R.id.dialog_changeip_fourEditText);
						
						String ipAddress = oneEdit.getText().toString() + "." + twoEdit.getText().toString() + "." + threeEdit.getText().toString() + "." + fourEdit.getText().toString();
						
						SharedPreferences mPreferences = getSharedPreferences("com.csi436.movieshelf", Context.MODE_PRIVATE);
				    	SharedPreferences.Editor editor = mPreferences.edit();
			        	editor.putString("server_ip", ipAddress);
			        	editor.commit();
						
						dialog.dismiss();
					}
				});
	 
				dialog.show();	
				return true;
				
		
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void checkFirstRun()
    {
    	
    	/***Checking first time ran***/
    	SharedPreferences mPreferences = this.getSharedPreferences("com.csi436.movieshelf", Context.MODE_PRIVATE);
        boolean firstTimeRan = mPreferences.getBoolean("firstTime", true);
        
       if(firstTimeRan && isNetworkAvailable() == true)
        {
    	    //We don't save the boolean here in case the user exits the app in the process
    	   
        	//Start Login Activity
        	Intent intent = new Intent(Activity_Home.this, Activity_Login.class);
        	startActivity(intent);
        	finish();
        }
       else if(isNetworkAvailable() == false)
    	   offlineMode = true;
    	
    	
    }
	
	private boolean isNetworkAvailable()
	{
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		
		return activeNetworkInfo != null;
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		new getMovies(this).execute();
	}
	
	private class getMovies extends AsyncTask<Void, Void, Boolean>
	{
		Context context;
		
		public getMovies(Context newContext)
		{
			context = newContext;
		}
		
		@Override
		protected Boolean doInBackground(Void... params)
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			
			try
			{
				SharedPreferences mPreferences = getSharedPreferences("com.csi436.movieshelf", Context.MODE_PRIVATE);
		        int userID = mPreferences.getInt("userID", -1);
		        String ipAddress = mPreferences.getString("server_ip", "192.168.1.100");
		        
		        if(userID == -1)
		        	return false;
		        
		        
		        HttpGet getthatSite = new HttpGet("http://" + ipAddress + ":58500/Users/"+ Integer.toString(userID) + "/MovieDetails");
				response = httpclient.execute(getthatSite);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK)
				{
					BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
					String json = reader.readLine();
					//Log.i("Response", json);
					try
					{
						JSONArray rottenReturn = new JSONArray(json);
						allMovies.clear();
						
						
						for(int i = 0; i < rottenReturn.length(); i++)
						{
							JSONObject allMovieInfo = rottenReturn.getJSONObject(i);
							//Log.i("Movie", allMovieInfo.toString());
							MovieItem newMovie = new MovieItem();
							
							/*****Movie Info Object*****/
							JSONObject movie = allMovieInfo.getJSONObject("MovieInfo");
							newMovie.setLibraryId(movie.getJSONObject("Movie").getInt("Id"));
							newMovie.setTitle(movie.getJSONObject("Movie").get("Title").toString());
							newMovie.setDescription(movie.getJSONObject("Movie").get("Description").toString());
							newMovie.setRTID(movie.getJSONObject("Movie").getInt("RTID"));
							
							if(movie.getJSONObject("Movie").isNull("Url"))
							{
								newMovie.setPhotoURL("");
							}
							else
								newMovie.setPhotoURL(movie.getJSONObject("Movie").get("Url").toString());
							
							if(!movie.getJSONObject("Movie").isNull("Year"))
								newMovie.setYear(movie.getJSONObject("Movie").getInt("Year"));
							
							/*****Actors Object *****/
							JSONArray actors = movie.getJSONArray("Actors");
							
							if(actors.length() >= 1)
								newMovie.setActorOne(actors.getJSONObject(0).getString("Name").toString());
							else
								newMovie.setActorOne("");
							
							if(actors.length() >= 2)
								newMovie.setActorTwo(actors.getJSONObject(1).getString("Name").toString());
							else
								newMovie.setActorOne("");
							
							/*****Director Object*****/
							JSONArray directors = movie.getJSONArray("Directors");
							
							if(directors.length() >= 1)
								newMovie.setDirector(directors.getJSONObject(0).getString("Name").toString());
							else
								newMovie.setDirector("");
							
							/***** Genre Object *****/
							JSONArray genres = movie.getJSONArray("Genres");
							
							for(int j = 0; j < genres.length(); j++)
							{
								if(newMovie.getGenre() == null)
									newMovie.setGenre(genres.getJSONObject(j).getString("Name"));
								else
									newMovie.setGenre(newMovie.getGenre() + ", " + genres.getJSONObject(j).getString("Name"));
							}
							
							
							//Get Rating
							newMovie.setRating(allMovieInfo.getInt("Rating"));
							
							//Get Comment
							if(allMovieInfo.isNull("Comment"))
								newMovie.setComment("");
							else
								newMovie.setComment(allMovieInfo.getString("Comment"));
							
							//Get if it's lent
							newMovie.setIsLent(allMovieInfo.getBoolean("IsLent"));
							
							//Get if it's viewed
							newMovie.setIsViewed(allMovieInfo.getBoolean("HasBeenViewed"));
							
							
							allMovies.add(newMovie);
						}
						
						return true;
						
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					
				}
				else
				{
					response.getEntity().getContent().close();
					throw new IOException(statusLine.getReasonPhrase());
				}
			} 
			catch (ClientProtocolException e)
			{
				return false;
			}
			catch (IOException e)
			{
				return false;
			}
			
			return false;
		}
		
		
		@Override
		protected void onPostExecute(final Boolean success)
		{
			if(success)
			{
				//MovieAdapter adapter = new MovieAdapter(a, R.layout.listitem_movie_home, allMovies);
				m_Adapter.notifyDataSetChanged();
			}
			else
			{
				Toast.makeText(context, "Something went wrong querying movies", Toast.LENGTH_SHORT).show();
			}
		}
		
	}
	
	private class lendMovie extends AsyncTask<Void, Void, Boolean>
	{
		Context context;
		boolean movieLent;
		int movieID;
		
		public lendMovie(Context c, boolean isLent, int newLibraryId)
		{
			context = c;
			movieLent = isLent;
			movieID = newLibraryId;
		}

		@Override
		protected Boolean doInBackground(Void... params)
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			
			try
			{
				SharedPreferences mPreferences = getSharedPreferences("com.csi436.movieshelf", Context.MODE_PRIVATE);
		        int userID = mPreferences.getInt("userID", -1);
		        String ipAddress = mPreferences.getString("server_ip", "192.168.1.100");
		        
		        if(userID == -1)
		        	return false;
		        
		        
		        String url = "http://" + ipAddress + ":58500/Users/"+ Integer.toString(userID) + "/Movies/" + movieID + "/IsLent/" + movieLent;
				Log.i("Url", url);
				HttpGet getthatSite = new HttpGet(url);
				response = httpclient.execute(getthatSite);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK)
				{
					BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
					String json = reader.readLine();
					Log.i("Response", json);
					
					if(json.equals("Could not change the status of movie lent."))
						return false;
					
					return true;
					
				}
				else
				{
					return false;
				}
			} 
			catch (ClientProtocolException e)
			{
				return false;
			}
			catch (IOException e)
			{
				return false;
			}
			
		}
		
		@Override
		protected void onPostExecute(final Boolean success)
		{
			if(success)
			{
				Toast.makeText(context, "Successfully changed lent", Toast.LENGTH_SHORT).show();
				new getMovies(context).execute();
			}
			else
			{
				Toast.makeText(context, "Oops, could not change", Toast.LENGTH_SHORT).show();
			}
				
			
		}
	}
	
	private class viewMovie extends AsyncTask<Void, Void, Boolean>
	{
		Context context;
		boolean movieViewed;
		int movieID;
		
		public viewMovie(Context c, boolean isViewed, int newLibraryId)
		{
			context = c;
			movieViewed = isViewed;
			movieID = newLibraryId;
		}

		@Override
		protected Boolean doInBackground(Void... params)
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			
			try
			{
				SharedPreferences mPreferences = getSharedPreferences("com.csi436.movieshelf", Context.MODE_PRIVATE);
		        int userID = mPreferences.getInt("userID", -1);
		        String ipAddress = mPreferences.getString("server_ip", "192.168.1.100");
		        
		        if(userID == -1)
		        	return false;
		        
		        
		        String url = "http://" + ipAddress + ":58500/Users/"+ Integer.toString(userID) + "/Movies/" + movieID + "/IsViewed/" + movieViewed;
				Log.i("Url", url);
				HttpGet getthatSite = new HttpGet(url);
				response = httpclient.execute(getthatSite);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK)
				{
					BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
					String json = reader.readLine();
					Log.i("Response", json);
					
					if(json.equals("Could not change the status of movie lent."))
						return false;
					
					return true;
					
				}
				else
				{
					return false;
				}
			} 
			catch (ClientProtocolException e)
			{
				return false;
			}
			catch (IOException e)
			{
				return false;
			}
			
		}
		
		@Override
		protected void onPostExecute(final Boolean success)
		{
			if(success)
			{
				Toast.makeText(context, "Successfully changed viewed", Toast.LENGTH_SHORT).show();
				new getMovies(context).execute();
			}
			else
			{
				Toast.makeText(context, "Oops, could not change", Toast.LENGTH_SHORT).show();
			}
				
			
		}
	}
	
	private class rateMovie extends AsyncTask<Void, Void, Boolean>
	{
		Context context;
		int movieRating;
		int movieID;
		
		public rateMovie(Context c, int newRating, int newLibraryId)
		{
			context = c;
			movieRating = newRating;
			movieID = newLibraryId;
		}

		@Override
		protected Boolean doInBackground(Void... params)
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response;
			
			try
			{
				SharedPreferences mPreferences = getSharedPreferences("com.csi436.movieshelf", Context.MODE_PRIVATE);
		        int userID = mPreferences.getInt("userID", -1);
		        String ipAddress = mPreferences.getString("server_ip", "192.168.1.100");
		        
		        if(userID == -1)
		        	return false;
		        
		        
		        String url = "http://" + ipAddress + ":58500/Users/"+ Integer.toString(userID) + "/Movies/" + movieID + "/Rating/" + movieRating;
				Log.i("Url", url);
				HttpGet getthatSite = new HttpGet(url);
				response = httpclient.execute(getthatSite);
				StatusLine statusLine = response.getStatusLine();
				if(statusLine.getStatusCode() == HttpStatus.SC_OK)
				{
					BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
					String json = reader.readLine();
					Log.i("Response", json);
					
					if(json.equals("Could not rate movie."))
						return false;
					
					return true;
					
				}
				else
				{
					return false;
				}
			} 
			catch (ClientProtocolException e)
			{
				return false;
			}
			catch (IOException e)
			{
				return false;
			}
			
		}
		
		@Override
		protected void onPostExecute(final Boolean success)
		{
			if(success)
			{
				Toast.makeText(context, "Successfully rated movie", Toast.LENGTH_SHORT).show();
				new getMovies(context).execute();
			}
			else
			{
				Toast.makeText(context, "Oops, could not change", Toast.LENGTH_SHORT).show();
			}
				
			
		}
	}
	
	
	/***End of activity***/
}
