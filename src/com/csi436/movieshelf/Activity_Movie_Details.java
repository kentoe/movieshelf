package com.csi436.movieshelf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.csi436.movieshelf.data.MovieItem;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Activity_Movie_Details extends FragmentActivity implements ActionBar.TabListener
{
	static SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	static ViewPager mViewPager;
	
	static MovieItem passedInMovie;
	
	static SharedPreferences mPreferences;
	
	static Context activityContext;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_movie_details);

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
        actionBar.setDisplayHomeAsUpEnabled(true);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
		{
			@Override
			public void onPageSelected(int position)
			{
				actionBar.setSelectedNavigationItem(position);
				
			}
		
		});
					

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++)
		{
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab().setText(mSectionsPagerAdapter.getPageTitle(i)).setTabListener(this));
		}
		
		
		mPreferences = getSharedPreferences("com.csi436.movieshelf", Context.MODE_PRIVATE);
		
        passedInMovie = (MovieItem) getIntent().getSerializableExtra("Movie");
        
        actionBar.setTitle(passedInMovie.getTitle());
        
        activityContext = this;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_movie_details, menu);
		return true;
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
	
	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
	{
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
	{
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
	{
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter
	{
		Fragment[] fragments = new Fragment[] { new Fragment_Details(), new Fragment_Comments() };

		public SectionsPagerAdapter(FragmentManager fm)
		{
			
			super(fm);
		}

		@Override
		public Fragment getItem(int position)
		{
			
			return fragments[position];
		}

		@Override
		public int getCount()
		{
			// Show 2 total pages.
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position)
		{
			Locale l = Locale.getDefault();
			switch (position)
			{
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			}
			return null;
		}
	}
	
	public static class Fragment_Details extends Fragment
	{

		Context fragmentContext;
		Button button_removeMovie;

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			View rootView = inflater.inflate(R.layout.fragment_movie_details, container, false);
			
			//TextView movieTitle = (TextView) rootView.findViewById(R.id.fragment_movie_details_TextViewTitle);
			TextView movieYear = (TextView) rootView.findViewById(R.id.fragment_movie_details_TextViewYear);
			RatingBar movieRating = (RatingBar) rootView.findViewById(R.id.fragment_movie_details_RatingBar);
			TextView movieDirector = (TextView) rootView.findViewById(R.id.fragment_movie_details_TextViewDirector);
			TextView movieGenre = (TextView) rootView.findViewById(R.id.fragment_movie_details_TextViewGenre);
			TextView movieDescription = (TextView) rootView.findViewById(R.id.fragment_movie_details_TextViewDescription);
			button_removeMovie = (Button) rootView.findViewById(R.id.fragment_movie_details_ButtonRemoveMovie);
			
			//Setting fields below			
			//movieTitle.setText(passedInMovie.getTitle());
			movieYear.setText(Integer.toString(passedInMovie.getYear()));
			
			float rating = (float) passedInMovie.getRating();
			if(rating == 0.0f)
			{
				movieRating.setVisibility(View.INVISIBLE);
			}
			else
			{
				movieRating.setVisibility(View.VISIBLE);
				movieRating.setRating(rating);
			}
			
			movieDirector.setText(passedInMovie.getDirector());
			movieGenre.setText(passedInMovie.getGenre());
			movieDescription.setText(passedInMovie.getDescription());
			movieDescription.setMovementMethod(new ScrollingMovementMethod());
			
			button_removeMovie.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					new deleteMovie(fragmentContext, passedInMovie.getLibraryId()).execute();
					
				}
				
			});
			
			
			
			
			return rootView;
		}

		
		@Override
		public void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
			fragmentContext = this.getActivity();
		}
		
		
		private class deleteMovie extends AsyncTask<Void, Void, Boolean>
	    {
	    	Context context;
	    	int movieLibraryId;
	    	
			public deleteMovie(Context c, int newLibraryID)
			{
				context = c;
				movieLibraryId = newLibraryID;
			}
			@Override
			protected Boolean doInBackground(Void... params)
			{
				HttpClient httpclient = new DefaultHttpClient();
				HttpResponse response;
				
				try
				{
			        int userID = mPreferences.getInt("userID", -1);
			        String ipAddress = mPreferences.getString("server_ip", "192.168.1.100");
			        
			        if(userID == -1)
			        	return false;
			        
					String url = "http://" + ipAddress + ":58500/Users/"+ Integer.toString(userID) + "/Movies/" + movieLibraryId + "/DeleteMovie/";
					Log.i("Url", url);
					HttpGet getthatSite = new HttpGet(url);
					response = httpclient.execute(getthatSite);
					StatusLine statusLine = response.getStatusLine();
					if(statusLine.getStatusCode() == HttpStatus.SC_OK)
					{
						BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
						String json = reader.readLine();
						Log.i("Response", json);
						
						if(json.equals("Movie does not exist."))
							return false;
						
						return true;
						
					}
					else
					{
						return false;
					}
					
					
				}
				catch (ClientProtocolException e)
				{
					return false;
				}
				catch (IOException e)
				{
					return false;
				}
			}
			
			@Override
			protected void onPostExecute(final Boolean success)
			{
				if(success)
				{
					Toast.makeText(context, "Successfully removed movie", Toast.LENGTH_SHORT).show();
					((Activity_Movie_Details)activityContext).finish();
					
				}
				else
				{
					Toast.makeText(context, "Oops, could not remove movie", Toast.LENGTH_SHORT).show();
				}
					
				
			}
	    }

	
	}
	
	public static class Fragment_Comments extends Fragment
	{
		TextView movie_commentText;
		Button movie_addComment;
		Button movie_changeComment;
		Button movie_removeComment;
		Context fragmentContext;
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			View rootView = inflater.inflate(R.layout.fragment_movie_details_comment, container, false);
			
			movie_commentText = (TextView) rootView.findViewById(R.id.fragment_movie_comment_personalCommentTextView);
			movie_addComment = (Button) rootView.findViewById(R.id.fragment_movie_comment_addCommentButton);
			movie_changeComment = (Button) rootView.findViewById(R.id.fragment_movie_comment_changeCommentButton);
			movie_removeComment = (Button) rootView.findViewById(R.id.fragment_movie_comment_removeCommentButton);
			
			String passedInComment = passedInMovie.getComment();
			
			if(passedInComment.isEmpty())
			{
				movie_addComment.setVisibility(View.VISIBLE);
				movie_changeComment.setVisibility(View.INVISIBLE);
				movie_removeComment.setVisibility(View.INVISIBLE);
				
			}
			else
			{
				movie_commentText.setText(passedInComment);
				movie_addComment.setVisibility(View.INVISIBLE);
				movie_changeComment.setVisibility(View.VISIBLE);
				movie_removeComment.setVisibility(View.VISIBLE);
				
			}
			
			movie_addComment.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					addComment();					
				}
				
			});
			
			movie_changeComment.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					changeComment();					
				}
				
			});
			
			movie_removeComment.setOnClickListener(new OnClickListener()
			{

				@Override
				public void onClick(View v)
				{
					removeComment();					
				}
				
			});
			
			
			return rootView;
		}
		
		@Override
		public void onCreate(Bundle savedInstanceState)
		{
			super.onCreate(savedInstanceState);
			fragmentContext = this.getActivity();
		}
		
		public void addComment()
		{
    		final Dialog dialog = new Dialog(fragmentContext);
			dialog.setContentView(R.layout.dialog_comment);
			dialog.setTitle("Add Comment");
 
			// set the custom dialog components - text, image and button
			final EditText commentText = (EditText) dialog.findViewById(R.id.dialog_comment_commentEditText);
			Button dialogButton = (Button) dialog.findViewById(R.id.dialog_comment_submitButton);
			// if button is clicked, close the custom dialog
			
			dialogButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) 
				{
					//Set Rating here
					String comment = commentText.getText().toString();
					
					new addCommentToMovie(fragmentContext, comment, passedInMovie.getLibraryId()).execute();
					
					dialog.dismiss();
				}
			});
 
			dialog.show();	
		}
		
		public void changeComment()
		{
			final Dialog dialog = new Dialog(fragmentContext);
			dialog.setContentView(R.layout.dialog_comment);
			dialog.setTitle("Change Comment");
 
			// set the custom dialog components - text, image and button
			final EditText commentText = (EditText) dialog.findViewById(R.id.dialog_comment_commentEditText);
			commentText.setText(movie_commentText.getText().toString());
			Button dialogButton = (Button) dialog.findViewById(R.id.dialog_comment_submitButton);
			// if button is clicked, close the custom dialog
			
			dialogButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) 
				{
					//Set Rating here
					String comment = commentText.getText().toString();
					
					new addCommentToMovie(fragmentContext, comment, passedInMovie.getLibraryId()).execute();
					
					dialog.dismiss();
				}
			});
 
			dialog.show();
			
		}
		
		public void removeComment()
		{
			new removeCommentToMovie(fragmentContext, passedInMovie.getLibraryId()).execute();
		}
		
		private class addCommentToMovie extends AsyncTask<Void, Void, Boolean>
		{
			Context context;
			String movieComment;
			int movieID;
			
			//Button movie_addComment = (Button) findViewById(R.id.fragment_movie_comment_addCommentButton);
			
			public addCommentToMovie(Context c, String newComment, int newLibraryId)
			{
				context = c;
				movieComment = newComment;
				movieID = newLibraryId;
			}

			@Override
			protected Boolean doInBackground(Void... params)
			{
				HttpClient httpclient = new DefaultHttpClient();
				HttpResponse response;
				
				try
				{
			        int userID = mPreferences.getInt("userID", -1);
			        String ipAddress = mPreferences.getString("server_ip", "192.168.1.100");
			        
			        if(userID == -1)
			        	return false;
			        
			        String passTheComment = movieComment.trim();
			        passTheComment = passTheComment.replaceAll(" ", "%20");
			        
			        String url = "http://" + ipAddress + ":58500/Users/"+ Integer.toString(userID) + "/Movies/" + movieID + "/Comment/" + passTheComment;
					Log.i("Url", url);
					HttpGet getthatSite = new HttpGet(url);
					response = httpclient.execute(getthatSite);
					StatusLine statusLine = response.getStatusLine();
					if(statusLine.getStatusCode() == HttpStatus.SC_OK)
					{
						BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
						String json = reader.readLine();
						Log.i("Response", json);
						
						/*
						if(json.equals("Could not rate movie."))
							return false;*/
						
						return true;
						
					}
					else
					{
						return false;
					}
				} 
				catch (ClientProtocolException e)
				{
					return false;
				}
				catch (IOException e)
				{
					return false;
				}
				
			}
			
			@Override
			protected void onPostExecute(final Boolean success)
			{
				if(success)
				{
					//Set movie comment, invisible add comment, visible change / remove
					movie_commentText.setText(movieComment);
					movie_addComment.setVisibility(View.INVISIBLE);
					movie_changeComment.setVisibility(View.VISIBLE);
					movie_removeComment.setVisibility(View.VISIBLE);
					Toast.makeText(context, "Successfully added comment", Toast.LENGTH_SHORT).show();
				}
				else
				{
					Toast.makeText(context, "Oops, could not add", Toast.LENGTH_SHORT).show();
				}
					
				
			}
		}
		
		private class removeCommentToMovie extends AsyncTask<Void, Void, Boolean>
		{
			Context context;
			int movieID;
			
			//Button movie_addComment = (Button) findViewById(R.id.fragment_movie_comment_addCommentButton);
			
			public removeCommentToMovie(Context c, int newLibraryId)
			{
				context = c;
				movieID = newLibraryId;
			}

			@Override
			protected Boolean doInBackground(Void... params)
			{
				HttpClient httpclient = new DefaultHttpClient();
				HttpResponse response;
				
				try
				{
			        int userID = mPreferences.getInt("userID", -1);
			        String ipAddress = mPreferences.getString("server_ip", "192.168.1.100");
			        
			        if(userID == -1)
			        	return false;
			        
			        
			        String url = "http://" + ipAddress + ":58500/Users/"+ Integer.toString(userID) + "/Movies/" + movieID + "/DeleteComment";
					Log.i("Url", url);
					HttpGet getthatSite = new HttpGet(url);
					response = httpclient.execute(getthatSite);
					StatusLine statusLine = response.getStatusLine();
					if(statusLine.getStatusCode() == HttpStatus.SC_OK)
					{
						BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
						String json = reader.readLine();
						Log.i("Response", json);
						
						/*
						if(json.equals("Could not rate movie."))
							return false;*/
						
						return true;
						
					}
					else
					{
						return false;
					}
				} 
				catch (ClientProtocolException e)
				{
					return false;
				}
				catch (IOException e)
				{
					return false;
				}
				
			}
			
			@Override
			protected void onPostExecute(final Boolean success)
			{
				if(success)
				{
					//Set movie comment, invisible add comment, visible change / remove
					movie_commentText.setText("");
					movie_addComment.setVisibility(View.VISIBLE);
					movie_changeComment.setVisibility(View.INVISIBLE);
					movie_removeComment.setVisibility(View.INVISIBLE);
					Toast.makeText(context, "Successfully removed comment", Toast.LENGTH_SHORT).show();
				}
				else
				{
					Toast.makeText(context, "Oops, could not add", Toast.LENGTH_SHORT).show();
				}
					
				
			}
		}
	
	
	
	
	}

}
