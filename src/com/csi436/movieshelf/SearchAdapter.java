package com.csi436.movieshelf;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.csi436.movieshelf.data.MovieItem;

public class SearchAdapter extends ArrayAdapter<MovieItem>
{
    
    Context context;
    int layoutResourceId;
    List<MovieItem> data = new ArrayList<MovieItem>();
    
    public SearchAdapter(Context context, int layoutResourceId, List<MovieItem> values)
    {
        super(context, layoutResourceId, values);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = values;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MovieHolder holder = null;
        
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            holder = new MovieHolder();
            holder.movieTitle = (TextView)row.findViewById(R.id.listitem_movie_titleTextView);
            holder.moviePhoto = (ImageView)row.findViewById(R.id.listitem_movie_imageView);
            holder.movieYear = (TextView)row.findViewById(R.id.listitem_movie_yearTextView);
            holder.movieActorOne = (TextView)row.findViewById(R.id.listitem_movie_actor1TextView);
            holder.movieActorTwo = (TextView)row.findViewById(R.id.listitem_movie_actor2TextView);
            
            row.setTag(holder);
        }
        else
        {
            holder = (MovieHolder)row.getTag();
        }
        
        MovieItem movie = data.get(position);
        
        holder.movieTitle.setText(movie.getTitle());
        holder.movieYear.setText(Integer.toString(movie.getYear()));
        holder.movieActorOne.setText(movie.getActorOne());
        holder.movieActorTwo.setText(movie.getActorTwo());
        
        String photoURL = movie.getPhotoURL();
        
        if(photoURL != null && !photoURL.isEmpty())
        {
            getBitmap newBitmap = new getBitmap(holder, movie.getPhotoURL());
            newBitmap.execute();
        }
        
        return row;
    }
    
    static class MovieHolder
    {
    	//Title, Description, RTID (Not displayed), PhotoURL, Actor 1 and 2
    	
        TextView movieTitle, movieYear, movieDescription, movieActorOne, movieActorTwo;
        
        ImageView moviePhoto;
        
    }
    

    
    private class getBitmap extends AsyncTask<Void, Void, Bitmap>
    {
    	MovieHolder currentHolder;
    	//String imageURL;
    	URI uri;
    	
    	public getBitmap(MovieHolder holder, String newURL)
    	{
    		super();
    		this.currentHolder = holder;
    		this.uri = URI.create(newURL);
    	}
		@Override
		protected Bitmap doInBackground(Void... params)
		{
			
			try
			{
				
				HttpClient httpClient = new DefaultHttpClient();
	  	        HttpGet httpGet = new HttpGet(uri);
	  	        HttpResponse httpResponse;
				httpResponse = httpClient.execute(httpGet);
				HttpEntity responseEntity = httpResponse.getEntity();
      	        InputStream input = responseEntity.getContent();
      	        Bitmap myBitmap = BitmapFactory.decodeStream(input);
      	        
      	        return myBitmap;
			} 
			catch (ClientProtocolException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
  	        return null;
		}
		
		@Override
        protected void onPostExecute(Bitmap result) 
		{
            super.onPostExecute(result);
            currentHolder.moviePhoto.setImageBitmap(result);
        }
    	
    }

}
